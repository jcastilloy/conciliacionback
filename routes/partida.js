var express = require('express');
var router = express.Router();

var partidaController = require('../controllers/partidasController');


router.post('/partidas', partidaController.partidas);
router.post('/create', partidaController.partida_create)
router.post('/update', partidaController.partida_update)
router.post('/delete', partidaController.partida_eliminar)

module.exports = router;