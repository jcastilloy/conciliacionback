"use strict";

var express = require('express');

var router = express.Router();

var cargaController = require('../controllers/cargaController.js');

router.post('/cargar-excel', cargaController.carga);
router.get('/detalle/:id', cargaController.ditail);
module.exports = router;