var express = require('express');
var router = express.Router();

var archivoController= require('../controllers/archivoController');

router.get('/archivos', archivoController.archivos);
router.post('/eliminar', archivoController.eliminar);
router.post('/crear', archivoController.archivo_create);


module.exports = router;