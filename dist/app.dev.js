"use strict";

var express = require('express');

var path = require('path');

var cookieParser = require('cookie-parser');

var logger = require('morgan');

var jwt = require('jsonwebtoken');

var cors = require('cors');

var bodyParser = require('body-parser');

require('./database/asociations');

var app = express();
app.use(cors()); // parse application/x-www-form-urlencoded

app.use(bodyParser.urlencoded({
  extended: false
})); // parse application/json

app.use(bodyParser.json());
app.use(bodyParser.json({
  limit: '50mb'
}));
app.use(bodyParser.urlencoded({
  limit: '50mb',
  extended: true
}));
app.use(express["static"](__dirname + "/"));
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.set('secretKey', 'jwt_pwd_d!sar3a-z4!1%%3#4567890');

var indexRouter = require('./routes/index'); //var usersRouter = require('./routes/users');


var cargarRouter = require('./routes/cargar');

var authApiRout = require('./routes/authRout');

var archivoRouter = require('./routes/archivo');

var partidaRouter = require('./routes/partida'); // app.use(logger('dev'));
// app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
// app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));


app.use('/', indexRouter);
app.use('/auth', authApiRout); //app.use('/users', validarUsuario, usersRouter);
// app.use('/carga',validarUsuario, cargarRouter);

app.use('/carga', validarUsuario, cargarRouter);
app.use('/archivo', validarUsuario, archivoRouter);
app.use('/partida', validarUsuario, partidaRouter); //funcion para verficiacion de que el usuario este logueado y con token valido

function validarUsuario(req, res, next) {
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function (err, decoded) {
    if (err) {
      res.json({
        status: "error",
        message: err.message,
        data: null
      });
    } else {
      req.body._userId = decoded.id;
      console.log('jwt verify ' + decoded);
      next();
    }
  });
}

module.exports = app;