"use strict";

var sequelize = require('./database/db');

var Archivo = require('./models/archivo');

var Grupo = require('./models/grupo');

var ArchivoDetalle = require('./models/archivo-detalle');

var EncabezadoColumna = require('./models/encabezado-columna');

require('./database/asociations');

var jsonObjet = {
  archivo: {
    nombre: "consiliacion1",
    extension: "xlsx",
    estado: true,
    grupos: [{
      nombre: "GT",
      EncabezadoColumnas: [{
        columna: "C1",
        descripcion: "Tarjeta",
        estado: true
      }, {
        columna: "C2",
        descripcion: "Bolsa de Valores",
        estado: true
      }],
      ArchivoDetalles: [{
        ctaContable: 101,
        nomContable: "Disponibilidad",
        ref: "A",
        C1: 11,
        C2: 12,
        C3: 13,
        C4: 14,
        C5: 15,
        C6: 16,
        C7: 17,
        C8: 18,
        tipoCambio: 1,
        debeReclas: 120,
        haberReclas: 125,
        debeAjuste: 200,
        haberAjuste: 205
      }, {
        ctaContable: 101,
        nomContable: "Disponibilidad",
        ref: "A",
        C1: 111,
        C2: 112,
        C3: 113,
        C4: 114,
        C5: 115,
        C6: 116,
        C7: 117,
        C8: 118,
        tipoCambio: 1,
        debeReclas: 120,
        haberReclas: 125,
        debeAjuste: 200,
        haberAjuste: 205
      }]
    }]
  }
};
sequelize.sync({
  force: false
}).then(function () {
  // Conexión establecida
  console.log("Conexión establecida...");
}).then(function _callee2() {
  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          arch = jsonObjet.archivo;
          _context2.next = 3;
          return regeneratorRuntime.awrap(Archivo.create(arch, {
            include: [{
              model: Grupo,
              as: 'grupos',
              include: [{
                model: EncabezadoColumna,
                as: 'EncabezadoColumnas'
              }, {
                model: ArchivoDetalle,
                as: 'ArchivoDetalles'
              }]
            }]
          }).then(function _callee(arch) {
            return regeneratorRuntime.async(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return regeneratorRuntime.awrap(EncabezadoColumna.update({
                      archivoId: arch.get('id')
                    }, {
                      where: {
                        archivoId: null
                      }
                    }));

                  case 2:
                    _context.next = 4;
                    return regeneratorRuntime.awrap(ArchivoDetalle.update({
                      archivoId: arch.get('id')
                    }, {
                      where: {
                        archivoId: null
                      }
                    }));

                  case 4:
                  case "end":
                    return _context.stop();
                }
              }
            });
          }, function (err) {
            console.log("=== err === ", err);
          }).then(function () {
            console.log("se ha insertado el archivo correctamente");
          }));

        case 3:
        case "end":
          return _context2.stop();
      }
    }
  });
});