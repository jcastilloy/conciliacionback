const { Model, DataTypes } = require('sequelize');
const sequelize = require('../database/db');

class ArchivoDetalle extends Model{}

ArchivoDetalle.init(
    {
        ctaContable: DataTypes.DECIMAL,
        nomContable: DataTypes.STRING,
        ref: DataTypes.STRING,
        C1: DataTypes.DECIMAL,
        C2: DataTypes.DECIMAL,
        C3: DataTypes.DECIMAL,
        C4: DataTypes.DECIMAL,
        C5: DataTypes.DECIMAL,
        C6: DataTypes.DECIMAL,
        C7: DataTypes.DECIMAL,
        C8: DataTypes.DECIMAL,
        tipoCambio: {
            type:DataTypes.DECIMAL,
            defaultValue: 1
        },
        debeReclas: DataTypes.DECIMAL,
        haberReclas: DataTypes.DECIMAL,
        debeAjuste: DataTypes.DECIMAL,
        haberAjuste: DataTypes.DECIMAL,
        grupoId: DataTypes.INTEGER,
        archivoId: DataTypes.INTEGER

    },
    {
        sequelize,
        modelName: 'ArchivoDetalle'
    }
);

module.exports = ArchivoDetalle;