const { Model, DataType, DataTypes } = require('sequelize');
const sequelize = require('../database/db');

class Partida extends Model{}

Partida.init(
    {
        ctaContable: DataTypes.DECIMAL,
        nomContable: DataTypes.STRING,
        debe: DataTypes.DOUBLE,
        haber: DataTypes.DOUBLE,
        tipo: DataTypes.STRING,
        estado: DataTypes.BOOLEAN,
        archivoId: DataTypes.DECIMAL,
        grupoId: DataTypes.DECIMAL
    },{
        sequelize,
        modelName: 'partida'
    }
);

module.exports = Partida;