"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var _require = require('sequelize'),
    Model = _require.Model,
    DataTypes = _require.DataTypes;

var sequelize = require('../database/db');

var ArchivoDetalle =
/*#__PURE__*/
function (_Model) {
  _inherits(ArchivoDetalle, _Model);

  function ArchivoDetalle() {
    _classCallCheck(this, ArchivoDetalle);

    return _possibleConstructorReturn(this, _getPrototypeOf(ArchivoDetalle).apply(this, arguments));
  }

  return ArchivoDetalle;
}(Model);

ArchivoDetalle.init({
  ctaContable: DataTypes.DECIMAL,
  nomContable: DataTypes.STRING,
  ref: DataTypes.STRING,
  C1: DataTypes.DECIMAL,
  C2: DataTypes.DECIMAL,
  C3: DataTypes.DECIMAL,
  C4: DataTypes.DECIMAL,
  C5: DataTypes.DECIMAL,
  C6: DataTypes.DECIMAL,
  C7: DataTypes.DECIMAL,
  C8: DataTypes.DECIMAL,
  tipoCambio: {
    type: DataTypes.DECIMAL,
    defaultValue: 1
  },
  debeReclas: DataTypes.DECIMAL,
  haberReclas: DataTypes.DECIMAL,
  debeAjuste: DataTypes.DECIMAL,
  haberAjuste: DataTypes.DECIMAL,
  grupoId: DataTypes.INTEGER,
  archivoId: DataTypes.INTEGER
}, {
  sequelize: sequelize,
  modelName: 'ArchivoDetalle'
});
module.exports = ArchivoDetalle;