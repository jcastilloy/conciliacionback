const { Model, DataTypes } = require('sequelize');
const sequelize = require('../database/db');


class EncabezadoColumna extends Model{}

EncabezadoColumna.init(
    {
        columna: DataTypes.STRING,
        descripcion: DataTypes.STRING,
        archivoId:DataTypes.INTEGER,
        estado: DataTypes.BOOLEAN

    },{
        sequelize,
        modelName: 'EncabezadoColumna'        
    }
);

module.exports = EncabezadoColumna;