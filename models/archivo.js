const { Model, DataTypes } = require('sequelize');
const sequelize = require('../database/db');

//nombre, existencia, fecha, estado

class Archivo extends Model{}

Archivo.init({
    nombre: DataTypes.STRING,
    extension: DataTypes.STRING,
    estado: DataTypes.BOOLEAN

},{
   sequelize,
   modelName:'archivo'
});

module.exports = Archivo;