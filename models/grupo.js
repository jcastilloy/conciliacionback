const { Model, DataTypes } = require('sequelize');
const  sequelize = require('../database/db')

class Grupo extends Model{}

Grupo.init({
    nombre: DataTypes.STRING
},{
    sequelize,
    modelName: 'grupo'
});

module.exports = Grupo;