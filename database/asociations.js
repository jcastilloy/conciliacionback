const Archivo  = require('../models/archivo');
const Grupo = require('../models/grupo');
const ArchivoDetalle = require('../models/archivo-detalle');
const EncabezadoColumna = require('../models/encabezado-columna');


//1aN
Archivo.hasMany(Grupo);
Grupo.belongsTo(Archivo);

Grupo.hasMany(EncabezadoColumna);
EncabezadoColumna.belongsTo(Grupo)


Grupo.hasMany(ArchivoDetalle);
ArchivoDetalle.belongsTo(Grupo)

//  EncabezadoColumna.hasMany(ArchivoDetalle);
// ArchivoDetalle.belongsTo(EncabezadoColumna);





