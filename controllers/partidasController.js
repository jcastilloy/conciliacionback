const Partida = require('../models/partida');
const sequelize = require('../database/db');

module.exports={
    partidas: async function(req, res, next){
        
        if(!req.body) res.status(400).send();

        await Partida.findAll({
            where: [
                    { archivoId: req.body.archivoId },
                    { grupoId: req.body.grupoId },
                    { estado: true }
            ]
        }).then(partidas=>{
            res.status(200).json(partidas);
        }).catch(err=>{
            res.status(500).send()
            console.log(err);
        });
    },
    partida_create: async function(req, res, next){
        data = req.body
        if(!data) res.status(400).send();
        await Partida.bulkCreate(
            data
        ).then(()=>{
            res.status(200).send();
        }).catch(err=>{
            res.status(501).send();
            console.log(err)
        });
    },
    buscar_id: async function(req, res, next){
        
        if(!req.body) res.status(400).send();

        await Partida.findOne({
            where:[
                    {archivoId: req.body.archivoId},
                    {grupoId: req.body.grupoId}
                ]
            
        }).then(partida=>{
            if(partida === null){
                res.status(204).send();
            }else{
                res.status(200).json(partida);
            }
        });
    },
    partida_update: async function(req, res, next){
        data = req.body;
        if(!data) res.status(400).send();
        try {
           await data.forEach(async fila=>{
                const [results, metadata] = await sequelize.query(
                    "update partidas set ctaContable="+ fila.ctaContable +
                    ", nomContable='"+ fila.nomContable +
                    "', debe="+ fila.debe +
                    ", haber="+ fila.haber +
                    ", tipo = '"+ fila.tipo +
                    "' where id ="+ fila.id
                    );
            });
            res.status(200).send();  
        } catch (error) {
            res.status(500).send();
        }
        
    },
    partida_eliminar: async function(req, res, next){

        ids = req.body.ids;
        if(!req.body.ids) res.status(400).send();
        await Partida.update(
            {estado: 0},
            {where:{id: ids}}
        ).then(()=>{
            res.status(204).send();
        }
        ).catch(err=>{
            res.status(500).send();
            console.log(err);
        });
    } 
};