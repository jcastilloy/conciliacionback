const sequelize = require('../database/db');
const Archivo = require('../models/archivo');
const Grupo = require('../models/grupo');
const EncabezadoColumna = require('../models/encabezado-columna');
const ArchivoDetalle = require('../models/archivo-detalle');


module.exports = {
    carga: async function(req, res, next){
        var arch = req.body.archivo;
        console.log(arch);
        await Archivo.create(
            arch,{
                include:[{
                    model: Grupo,
                    as:'grupos', include:[{
                                model: EncabezadoColumna,
                                as:'EncabezadoColumnas'
                            },{
                                model:ArchivoDetalle,
                                as: 'ArchivoDetalles'
                    }]
                }]
            }
            
        ).then(async function(arch){
            await EncabezadoColumna.update({
                archivoId: arch.get('id')
            },{
                where:{archivoId: null}
            });
            
            await ArchivoDetalle.update({
                archivoId: arch.get('id')
            },{
                where: {archivoId: null}
            });
        }, function(err){
            res.status(501).json({error: err});
            console.log("=== err === ", err);
        }).then(()=>{
            res.status(201).json({mensaje: 'Se ha almacenado el archivo'});
        });
    
    },
        ditail: function(req, res, next){
            archivoId = req.params.id;
            if (!archivoId) res.status(404).send();
            sequelize.query('call getFieldDetail(' + archivoId + ')').then(data =>{
                res.status(200).json({data});
        });
    }
}

