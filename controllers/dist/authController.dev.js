"use strict";

var bcrypt = require('bcrypt');

var jwt = require('jsonwebtoken');

module.exports = {
  login: function login(req, res, next) {
    try {
      var idUsuario = req.body.id; // res.json({id: req.body.id});

      if (!idUsuario.empty) {
        var token = jwt.sign({
          foo: 'bar'
        }, req.app.get('secretKey'), {
          expiresIn: '1h'
        });
        res.status(200).json({
          message: 'Usuario encontrado',
          data: {
            id: idUsuario,
            token: token
          }
        });
        res.status(401).json({
          message: "Usuario / Password invalido"
        });
        return;
      }
    } catch (error) {
      res.json(error);
      return;
    }
  }
};