"use strict";

var sequelize = require('../database/db');

var Archivo = require('../models/archivo');

var Grupo = require('../models/grupo');

var EncabezadoColumna = require('../models/encabezado-columna');

var ArchivoDetalle = require('../models/archivo-detalle');

module.exports = {
  carga: function carga(req, res, next) {
    var arch;
    return regeneratorRuntime.async(function carga$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            arch = req.body.archivo;
            console.log(arch);
            _context2.next = 4;
            return regeneratorRuntime.awrap(Archivo.create(arch, {
              include: [{
                model: Grupo,
                as: 'grupos',
                include: [{
                  model: EncabezadoColumna,
                  as: 'EncabezadoColumnas'
                }, {
                  model: ArchivoDetalle,
                  as: 'ArchivoDetalles'
                }]
              }]
            }).then(function _callee(arch) {
              return regeneratorRuntime.async(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return regeneratorRuntime.awrap(EncabezadoColumna.update({
                        archivoId: arch.get('id')
                      }, {
                        where: {
                          archivoId: null
                        }
                      }));

                    case 2:
                      _context.next = 4;
                      return regeneratorRuntime.awrap(ArchivoDetalle.update({
                        archivoId: arch.get('id')
                      }, {
                        where: {
                          archivoId: null
                        }
                      }));

                    case 4:
                    case "end":
                      return _context.stop();
                  }
                }
              });
            }, function (err) {
              res.status(501).json({
                error: err
              });
              console.log("=== err === ", err);
            }).then(function () {
              res.status(201).json({
                mensaje: 'Se ha almacenado el archivo'
              });
            }));

          case 4:
          case "end":
            return _context2.stop();
        }
      }
    });
  },
  ditail: function ditail(req, res, next) {
    archivoId = req.params.id;
    if (!archivoId) res.status(404).send();
    sequelize.query('call getFieldDetail(' + archivoId + ')').then(function (data) {
      res.status(200).json({
        data: data
      });
    });
  }
};