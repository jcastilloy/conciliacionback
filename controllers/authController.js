const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');


module.exports ={
    login: function(req, res, next){
        try {
            var idUsuario =req.body.id;
                // res.json({id: req.body.id});
            if (!idUsuario.empty) {
                const token = jwt.sign({foo:'bar'},req.app.get('secretKey'),{expiresIn:'1h'});
                res.status(200).json({message:'Usuario encontrado', data:{id:idUsuario,token:token}});
                res.status(401).json({message:"Usuario / Password invalido"});
                return;
            }
        } catch (error) {
            res.json(error);
            return;
        }
        
    }
};
