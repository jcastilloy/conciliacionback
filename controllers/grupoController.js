const Grupo = require('../models/grupo');

module.exports = {
    grupos: async function(req, res, next){
        await Grupo.findAll().then(grupos=>{
            res.status(200).json(grupos);
        });
    },
};