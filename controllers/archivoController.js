const Archivo = require('../models/archivo');


module.exports = {
    archivos: async function(req, res, next){
        await Archivo.findAll({where:{estado:1}}).then(archivo=>{
            res.status(200).json(archivo);
        });
    },
    eliminar: async function(req, res, next){
        //no se elimina el registro, solo se deshabilita
        if(!req.body) res.status(400).send();
        var id = req.body.id;
        await Archivo.update(
            { estado: 0 }, 
            {where: {id: id}}
        ).then(()=>{
            res.status(204).send();
        }).catch((err)=>{
            res.status(501).send();
            console.log(err);
        });
    },
    archivo_create: async function(req, res, next){
        
        if(!req.body) res.status(400).send();
        
        var archivo = new Archivo(req.body.id, req.body.nombre, req.body.extension, req.body.status);
        await Archivo.create(archivo).then(()=>{
            res.status(200).send();
        }).catch((err)=>{
            res.status(501).send();
            console.log(err);
        });
    }
}