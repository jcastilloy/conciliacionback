const sequelize = require('./database/db');

const Archivo = require('./models/archivo');
const Grupo = require('./models/grupo');
const ArchivoDetalle = require('./models/archivo-detalle');
const EncabezadoColumna = require('./models/encabezado-columna');
require('./database/asociations');


var jsonObjet = {
	archivo: {
		nombre: "consiliacion1",
		extension: "xlsx",
		estado: true,
		grupos: [{
			nombre: "GT",
			EncabezadoColumnas: [{
					columna: "C1",
					descripcion: "Tarjeta",
					estado: true
				},
				{
					columna: "C2",
					descripcion: "Bolsa de Valores",
					estado: true
				}
			],
			ArchivoDetalles: [{
					ctaContable: 101,
					nomContable: "Disponibilidad",
					ref: "A",
					C1: 11,
					C2: 12,
					C3: 13,
					C4: 14,
					C5: 15,
					C6: 16,
					C7: 17,
					C8: 18,
					tipoCambio: 1,
					debeReclas: 120,
					haberReclas: 125,
					debeAjuste: 200,
					haberAjuste: 205
				},
				{
					ctaContable: 101,
					nomContable: "Disponibilidad",
					ref: "A",
					C1: 111,
					C2: 112,
					C3: 113,
					C4: 114,
					C5: 115,
					C6: 116,
					C7: 117,
					C8: 118,
					tipoCambio: 1,
					debeReclas: 120,
					haberReclas: 125,
					debeAjuste: 200,
					haberAjuste: 205
				}
			]
		}]
	}

};


sequelize.sync({ force: false }).then(() => {
    // Conexión establecida
    console.log("Conexión establecida...");
}).then(async ()=>{
    arch = jsonObjet.archivo;
    await Archivo.create(
        arch,{
            include:[{
				model: Grupo,
                as:'grupos', include:[{
                            model: EncabezadoColumna,
                            as:'EncabezadoColumnas'
                        },{
                            model:ArchivoDetalle,
                            as: 'ArchivoDetalles'
                }]
            }]
		}
        
    ).then(async function(arch){
		// console.log(arch.get('id'));
		//actualizar donde el archivo es nulo
		await EncabezadoColumna.update({
			archivoId: arch.get('id')
		},{
			where:{archivoId: null}
		});
		
		await ArchivoDetalle.update({
			archivoId: arch.get('id')
		},{
			where: {archivoId: null}
		});
    }, function(err){
        console.log("=== err === ", err);
    }).then(()=>{
		console.log("se ha insertado el archivo correctamente");
	});
});